let timeout;
const serverRequestDelay = 3000;
const inputElement       = document.getElementById("urlInput");
const outputElement      = document.getElementById("outputUrlCheck");


function isUrlFormatValid(url){
    // very simple regex, allowing cyrillic letters, chinese signs etc.
    let urlRegex = /^https?:\/\/.+?\..+?$/;
    return urlRegex.test(url);
}


async function getDetailsAboutUrl(url){

    // Simulating network latency of 1 second
    await new Promise(resolve => setTimeout(resolve, 1000));

    let randomNumber = Math.floor(Math.random() * 3); // 0, 1, or 2
    // Faking a JSON response
    let result = {
        version: "1.0",
        url: url,
        responseCode: randomNumber.toString(),
        responseMessage: ""
    };

    switch(randomNumber) {
        case 0:
            result.responseMessage = "The URL does not exist";
            break;
        case 1:
            result.responseMessage = "The URL points to a file";
            break;
        case 2:
            result.responseMessage = "The URL points to a directory";
            break;
        default:
            result.responseMessage = "Something went wrong";
    }
    return result;
}


function makeOutput(outputElement, outputText){
    outputElement.textContent = outputText;
}


inputElement.addEventListener("input", () => {
    handleInput(inputElement.value, outputElement, serverRequestDelay);
})


function handleResponse(result, outputElement){
    if (!result.responseCode){
        console.log("Server response does not contain the responseCode");
        return;
    }
    if (!result.url){
        console.log("Server response does not contain the url");
        return;
    }
    // the server repsonse is deprecated
    if (inputElement.value != result.url){
        //console.log("DEBUG: the repsonse is out-to-date");
        return;
    }

    // the server response is up-to-date
    let outputMessage = "";
    switch(result.responseCode) {
        // responseCode is a string
        case "0":
            outputMessage = "Die URL existiert nicht";
            break;
        case "1":
            outputMessage = "Die URL zeigt auf eine Datei";
            break;
        case "2":
            outputMessage = "Die URL zeigt auf einen Ordner";
            break;
        default:
            outputMessage = "Etwas ist schiefgelaufen";
    }
    makeOutput(outputElement, outputMessage);
}


function handleInput(url, outputElement, serverRequestDelay){
    clearTimeout(timeout);
    if (url == "") {
        makeOutput(outputElement, "");
    }
    else if (!isUrlFormatValid(url)) {
        makeOutput(outputElement,
            "Das URL-Format ist nicht korrekt.");
    }
    else {
        makeOutput(outputElement,
            "Das URL-Format ist korrekt.");

        // waiting some time before requesting the server
        timeout = setTimeout(() => {
            makeOutput(outputElement,
                "Das URL-Format ist korrekt. Bitte warte...");
            getDetailsAboutUrl(url).then(
                function(value) {handleResponse(value, outputElement);},
                function(error) {console.log(error);}
            )
        }, serverRequestDelay);
    }
    return;
}
